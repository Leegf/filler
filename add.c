/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/20 19:23:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/23 19:00:11 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void	free_token(char **token)
{
	int i;

	i = 0;
	while (token[i])
	{
		free(token[i]);
		i++;
	}
	free(token[i]);
	free(token);
	token = NULL;
}

void	free_map(char **map)
{
	int i;

	i = 0;
	while (map[i])
	{
		free(map[i]);
		i++;
	}
	free(map[i]);
	free(map);
	map = NULL;
}

void	fill_map(char **map, t_map_dim map_dim, int flag)
{
	int		count1;
	char	*tmp;

	count1 = 0;
	if (flag)
	{
		get_next_line(0, &tmp);
		ft_memdel((void **)&tmp);
	}
	while (count1 < map_dim.x)
	{
		get_next_line(0, &tmp);
		map[count1] = ft_strcpy(map[count1], tmp + 4);
		ft_memdel((void **)&tmp);
		count1++;
	}
	map[count1] = 0;
}

void	check_the_player(int *x)
{
	char *tmp;

	get_next_line(0, &tmp);
	*x = ft_atoi(&tmp[(ft_strchr(tmp, 'p') - tmp) + 1]);
	ft_memdel((void **)&tmp);
}
