/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visualize3.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/24 14:43:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/24 15:01:35 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include "filler.h"

void	draw_a_square(void *mlx_ptr, void *win_ptr, t_map_dim coords)
{
	int height;
	int width;
	int count;
	int count2;

	count = 0;
	height = coords.y * PIXEL_H + BORDER * coords.y + 1;
	width = coords.x * PIXEL_W + BORDER * coords.x + 1;
	while (count < PIXEL_W)
	{
		count2 = 0;
		while (count2 < PIXEL_H)
		{
			mlx_pixel_put(mlx_ptr, win_ptr, count + height, count2 + width,
						FLD_COLOR);
			count2++;
		}
		count++;
	}
}

void	draw_a_square2(void *mlx_ptr, void *win_ptr, t_map_dim coords,
					uint32_t col)
{
	int height;
	int width;
	int count;
	int count2;

	count = 0;
	height = coords.y * PIXEL_H + BORDER * coords.y + 1;
	width = coords.x * PIXEL_W + BORDER * coords.x + 1;
	while (count < PIXEL_W)
	{
		count2 = 0;
		while (count2 < PIXEL_H)
		{
			mlx_pixel_put(mlx_ptr, win_ptr, count + height, count2 + width,
						col);
			count2++;
		}
		count++;
	}
}
