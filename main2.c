/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 19:25:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/21 20:39:46 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		find_len(t_map_dim coords, int i, int j)
{
	int rez;
	int p1;
	int p2;

	p1 = ABS((i - coords.y));
	p2 = ABS((j - coords.x));
	rez = p1 + p2;
	return (rez);
}

void	find_o(char **tmp, int x, t_flist **lst, t_map_dim coords)
{
	size_t	i;
	size_t	j;
	int		min;
	int		len;

	i = 0;
	x = ft_tolower(x) == 'x' ? 'o' : 'x';
	min = 0;
	while (tmp[i])
	{
		j = 0;
		while (tmp[i][j])
		{
			if (ft_tolower(tmp[i][j]) == (char)x)
			{
				len = find_len(coords, i, j);
				min = min < len ? len : min;
			}
			j++;
		}
		i++;
	}
	ft_push_flist(lst, coords.x, coords.y, min);
}

/*
** Checks whether it's possible to place a token into the particular place of
** map.
*/

int		check_placement(t_map_dim coords, t_map_dim coords_t, char **token,
						char **map)
{
	int			i;
	int			j;
	t_map_dim	damn;

	i = 0;
	while (token[i])
	{
		j = 0;
		while (token[i][j])
		{
			if (token[i][j] == '*')
			{
				damn.x = j;
				damn.y = i;
				if (!check_map(map, damn, coords, coords_t))
				{
					return (0);
				}
			}
			j++;
		}
		i++;
	}
	return (1);
}

int		place_token(t_flist **lst, char **map, char **token)
{
	t_map_dim	count;
	t_flist		*tmp;

	norm_init(&tmp, &count, lst);
	while (tmp)
	{
		count.y = -1;
		while (token[++count.y] && (count.x = -1))
		{
			while (token[count.y][++count.x])
			{
				if (token[count.y][count.x] == '*')
				{
					if (norm(count, tmp, token, map))
					{
						flist_clear(lst);
						return (1);
					}
				}
			}
		}
		tmp = tmp->next;
	}
	flist_clear(lst);
	return (0);
}

int		crush_everyone(char **map, char **token, int x)
{
	size_t		i;
	size_t		j;
	t_flist		*lst;
	t_map_dim	coords;

	i = 0;
	lst = NULL;
	while (map[i])
	{
		j = 0;
		while (map[i][j])
		{
			if (ft_tolower(map[i][j]) == (char)x)
			{
				coords.y = i;
				coords.x = j;
				find_o(map, x, &lst, coords);
			}
			j++;
		}
		i++;
	}
	sort_flist(&lst);
	return (place_token(&lst, map, token));
}
