/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main3.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/17 16:54:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/22 15:44:19 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void		give_the_coords(t_map_dim coords, t_map_dim coords_t)
{
	while (coords_t.y)
	{
		coords_t.y--;
		coords.y--;
	}
	while (coords_t.x)
	{
		coords_t.x--;
		coords.x--;
	}
	ft_printf("%d %d\n", coords.y, coords.x);
}

int			norm(t_map_dim count, t_flist *lst, char **token, char **map)
{
	t_options opt;
	t_options ord;

	fill_coords(&ord, lst, count.y, count.x);
	if (check_placement(ord.cor, ord.cor_t, token, map))
	{
		cpy_struct(&opt, ord);
		return (1);
	}
	return (0);
}

void		norm_init(t_flist **hm, t_map_dim *count, t_flist **lst)
{
	*hm = *lst;
	count->x = 0;
	count->y = 0;
}
