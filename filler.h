/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/22 16:20:48 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/24 15:41:25 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLER_H
# define FILLER_H

# include "libft/includes/ft_printf.h"

# define ABS(x) x > 0 ? x : (x * (-1))
# define PIXEL_H 12
# define PIXEL_W 12
# define BORDER 1
# define BRD_COLOR 0x892727
# define FLD_COLOR 0xf98500
# define X_CLR 0x001f51
# define X_CLRL 0x0f52bf
# define O_CLR 0x5b0001
# define O_CLRL 0xc1282a

typedef struct	s_map_dim
{
	int x;
	int y;
}				t_map_dim;

typedef struct	s_flist
{
	int				cor_x;
	int				cor_y;
	int				len;
	struct s_flist	*next;
}				t_flist;

typedef struct	s_options
{
	t_map_dim cor;
	t_map_dim cor_t;
}				t_options;

typedef struct	s_graph
{
	void	*win_ptr;
	void	*mlx_ptr;
}				t_graph;

int				crush_everyone(char **map, char **token, int x);
void			ft_push_flist(t_flist **tmp, int x, int y, int l);
void			read_map_n_token(char ***map, char ***token, int flag);
void			merge_flist(t_flist *l, t_flist *r, t_flist **head);
void			give_the_coords(t_map_dim coords, t_map_dim coords_t);
int				check_map(char **map, t_map_dim damn, t_map_dim coords,
							t_map_dim coords_t);
void			flist_clear(t_flist **alst);
void			free_token(char **token);
void			free_map(char **map);
void			fill_map(char **map, t_map_dim map_dim, int flag);
void			allocate_memory_for_map(char ***map, t_map_dim *map_dim);
void			check_the_player(int *x);
void			make_token(char ***token);
void			fill_coords(t_options *ord, t_flist *lst, int i, int j);
void			cpy_struct(t_options *opt, t_options ord);
void			sort_flist(t_flist **head);
int				check_placement(t_map_dim coords, t_map_dim coords_t,
							char **token, char **map);
int				norm(t_map_dim count, t_flist *lst, char **token, char **map);
void			norm_init(t_flist **hm, t_map_dim *count, t_flist **lst);
void			draw_a_square(void *mlx_ptr, void *win_ptr, t_map_dim coords);
void			draw_field(void *mlx_ptr, void *win_ptr, t_options dims);
int				exity(void *ptr);
int				exit_esc(int keyboard, void *ptr);
void			draw_a_square2(void *mlx_ptr, void *win_ptr, t_map_dim coords,
							uint32_t col);
#endif
