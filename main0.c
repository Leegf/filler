/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main0.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/22 21:35:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/24 15:03:28 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		main(void)
{
	char	**map;
	char	**token;
	int		x;
	int		flag;

	flag = 0;
	check_the_player(&x);
	x = x == 1 ? 'o' : 'x';
	while (42)
	{
		if (flag)
			free_token(token);
		read_map_n_token(&map, &token, flag);
		flag++;
		if (!crush_everyone(map, token, x))
		{
			free_map(map);
			ft_printf("-1 -1\n");
			break ;
		}
	}
	return (0);
}
