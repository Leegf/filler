/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visualize2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/24 14:37:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/24 15:00:17 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include "filler.h"

void	fill_field(void *mlx_ptr, void *win_ptr, t_options dims)
{
	t_map_dim coords;

	coords.y = 0;
	while (coords.y < dims.cor.x)
	{
		coords.x = 0;
		while (coords.x < dims.cor.y)
		{
			draw_a_square(mlx_ptr, win_ptr, coords);
			coords.x++;
		}
		coords.y++;
	}
}

void	draw_field2(void *mlx_ptr, void *win_ptr, t_options dims)
{
	int i;
	int pos;

	i = 0;
	while (i < dims.cor.x)
	{
		pos = 0;
		while (pos < dims.cor_t.y && i > 0)
		{
			mlx_pixel_put(mlx_ptr, win_ptr, i * PIXEL_W + BORDER * i, pos,
						BRD_COLOR);
			pos++;
		}
		i++;
	}
	fill_field(mlx_ptr, win_ptr, dims);
}

void	draw_field(void *mlx_ptr, void *win_ptr, t_options dims)
{
	int i;
	int pos;

	i = 0;
	while (i < dims.cor.y)
	{
		pos = 0;
		while (pos < dims.cor_t.x && i > 0)
		{
			mlx_pixel_put(mlx_ptr, win_ptr, pos, i * PIXEL_H + BORDER * i,
						BRD_COLOR);
			pos++;
		}
		i++;
	}
	draw_field2(mlx_ptr, win_ptr, dims);
}

int		exity(void *ptr)
{
	mlx_destroy_window(((t_graph *)ptr)->mlx_ptr, ((t_graph *)ptr)->win_ptr);
	exit(1);
	return (1);
}

int		exit_esc(int keyboard, void *ptr)
{
	if (keyboard == 53)
	{
		ptr = NULL;
		exit(1);
	}
	return (1);
}
