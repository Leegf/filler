/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 20:25:46 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/24 15:02:30 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

/*
** Simply reads appropriate line of the line and thus allocate needed memory.
*/

static int g_max_x;
static int g_max_y;

void	allocate_memory_for_map(char ***map, t_map_dim *map_dim)
{
	int		count;
	char	*tmp;
	char	*tmp2;

	get_next_line(0, &tmp);
	map_dim->x = ft_atoi(ft_strchr(tmp, ' '));
	g_max_x = map_dim->x;
	count = 0;
	tmp2 = ft_itoa(map_dim->x);
	map_dim->y = ft_atoi(ft_strchr(tmp, ' ') +
						ft_strlen(tmp2) + 1);
	g_max_y = map_dim->y;
	*map = malloc((map_dim->x * sizeof(char *)) + 1);
	ft_memdel((void **)&tmp);
	ft_memdel((void **)&tmp2);
	while (count < map_dim->x)
		(*map)[count++] = ft_strnew(map_dim->y);
}

/*
** Puts the map into double array previously allocated.
*/

int		check_map(char **map, t_map_dim damn, t_map_dim coords,
					t_map_dim coords_t)
{
	if (damn.y == coords_t.y && damn.x == coords_t.x)
		return (1);
	if (((coords.y + (damn.y - coords_t.y)) >= 0 && (coords.y + (damn.y
	- coords_t.y)) < g_max_x - 1) && ((coords.x + (damn.x - coords_t.x))
		>= 0 && (coords.x + (damn.x - coords_t.x) < g_max_y - 1)))
	{
		if (ft_tolower(map[coords.y + (damn.y - coords_t.y)][coords.x +
				(damn.x - coords_t.x)]) == 'o' ||
			ft_tolower(map[coords.y + (damn.y - coords_t.y)][coords.x +
					(damn.x - coords_t.x)]) == 'x')
			return (0);
		else
			return (1);
	}
	return (0);
}

/*
** Allocates and at the same time fills double array for token.
*/

void	read_map_n_token(char ***map, char ***token, int flag)
{
	char			*tmp;
	t_map_dim		map_dim;

	if (!flag)
		allocate_memory_for_map(map, &map_dim);
	else
	{
		map_dim.x = g_max_x;
		map_dim.y = g_max_y;
	}
	get_next_line(0, &tmp);
	ft_memdel((void **)&tmp);
	fill_map(*map, map_dim, flag);
	make_token(token);
}

void	make_token(char ***token)
{
	char	*tmp;
	char	*tmp2;
	int		x;
	int		y;
	int		count;

	count = 0;
	get_next_line(0, &tmp);
	x = ft_atoi(ft_strchr(tmp, ' '));
	tmp2 = ft_itoa(x);
	y = ft_atoi(ft_strchr(tmp, ' ') + ft_strlen(tmp2) + 1);
	*token = malloc((x * sizeof(char *)) + 1);
	while (count < x)
		(*token)[count++] = ft_strnew(y);
	ft_memdel((void **)&tmp);
	ft_memdel((void **)&tmp2);
	count = 0;
	while (count < x)
	{
		get_next_line(0, &tmp);
		(*token)[count] = ft_strcpy((*token)[count], tmp);
		ft_memdel((void **)&tmp);
		count++;
	}
	(*token)[count] = 0;
}

/*
** This, where parsing/allocating part takes place.
*/
