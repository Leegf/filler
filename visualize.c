/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visualize.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/22 16:45:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/24 15:04:13 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include "filler.h"

static t_options	g_dims;
static int			g_flag;
static int			g_not_read;

t_options			create_a_window(void *mlx_ptr, void **win_ptr)
{
	char		*tmp;
	char		*tmp2;
	t_options	dims;

	get_next_line(0, &tmp);
	dims.cor.y = ft_atoi(ft_strchr(tmp, ' '));
	tmp2 = ft_itoa(dims.cor.y);
	dims.cor.x = ft_atoi(ft_strchr(tmp, ' ') +
						ft_strlen(tmp2) + 1);
	ft_memdel((void **)&tmp);
	ft_memdel((void **)&tmp2);
	dims.cor_t.x = ((dims.cor.x * PIXEL_H) + (BORDER * dims.cor.x)) - BORDER;
	dims.cor_t.y = (((dims.cor.y - 1) * PIXEL_W) + (BORDER * dims.cor.y)) -
				BORDER;
	*win_ptr = mlx_new_window(mlx_ptr, dims.cor_t.x, dims.cor_t.y, "filler");
	return (dims);
}

void				allocate_map_for_vis(char ***map, t_map_dim coords)
{
	int		count;

	count = 0;
	*map = malloc((coords.x * sizeof(char *)) + 1);
	while (count < coords.x)
		(*map)[count++] = ft_strnew(coords.y);
}

void				draw_map(char **map, void *ptr)
{
	t_map_dim coords;

	coords.x = 0;
	while (map[coords.x])
	{
		coords.y = 0;
		while (map[coords.x][coords.y])
		{
			if (map[coords.x][coords.y] == 'X')
				draw_a_square2(((t_graph *)ptr)->mlx_ptr,
							((t_graph *)ptr)->win_ptr, coords, X_CLR);
			else if (map[coords.x][coords.y] == 'x')
				draw_a_square2(((t_graph *)ptr)->mlx_ptr,
							((t_graph *)ptr)->win_ptr, coords, X_CLRL);
			else if (map[coords.x][coords.y] == 'O')
				draw_a_square2(((t_graph *)ptr)->mlx_ptr,
							((t_graph *)ptr)->win_ptr, coords, O_CLR);
			else if (map[coords.x][coords.y] == 'o')
				draw_a_square2(((t_graph *)ptr)->mlx_ptr,
							((t_graph *)ptr)->win_ptr, coords, O_CLRL);
			coords.y++;
		}
		coords.x++;
	}
}

int					draw_filler(void *ptr)
{
	char			*tmp;
	static char		**map;
	int				tmp2;

	if (g_not_read)
		return (0);
	if (!g_flag)
	{
		tmp2 = g_dims.cor.x;
		g_dims.cor.x = g_dims.cor.y;
		g_dims.cor.y = tmp2;
		allocate_map_for_vis(&map, g_dims.cor);
	}
	while (++g_flag && get_next_line(0, &tmp))
	{
		if (ft_strnequ(tmp, "000", 3))
		{
			fill_map(map, g_dims.cor, 0);
			draw_map(map, ptr);
		}
		else if (ft_strnequ(tmp, "==", 2))
			g_not_read = 1;
		return (0);
	}
	return (0);
}

int					main(void)
{
	char		*tmp;
	int			count;
	void		*mlx_ptr;
	void		*win_ptr;
	t_graph		ptr;

	count = 0;
	while (count < 9)
	{
		get_next_line(0, &tmp);
		ft_memdel((void **)&tmp);
		count++;
	}
	mlx_ptr = mlx_init();
	g_dims = create_a_window(mlx_ptr, &win_ptr);
	draw_field(mlx_ptr, win_ptr, g_dims);
	get_next_line(0, &tmp);
	ft_memdel((void **)&tmp);
	ptr.win_ptr = win_ptr;
	ptr.mlx_ptr = mlx_ptr;
	mlx_key_hook(win_ptr, exit_esc, (void *)(&ptr));
	mlx_hook(win_ptr, 17, 1L << 17, exity, (void *)(&ptr));
	mlx_loop_hook(mlx_ptr, draw_filler, (void *)(&ptr));
	mlx_loop(mlx_ptr);
	return (0);
}
