In this project I had to create my bot for game called `filler`. It had to compete with other bots. Game was processed by virtual machine `filler_vm`. You may find files of other bots and the virtual machine at `develop` branch. Virtual machine works only on mac. Check filler.en.pdf in develop branch for more details.

To compile: `make`.

To compile visual part: `make visual`.

Other commands: `make re`, `make clean`, `make fclean`
