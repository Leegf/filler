# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/06 12:02:16 by lburlach          #+#    #+#              #
#    Updated: 2018/03/24 15:40:22 by lburlach         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY: all clean fclean re

NAME = lburlach.filler
OBJ = main0.o main.o main2.o main3.o add.o flist.o flist2.o 
OBJ_S = main.o main2.o main3.o add.o flist.o flist2.o 
OBJ_V = visualize.o visualize2.o visualize3.o
CC = gcc
CFLAGS = -Wall -Wextra -Werror
CURDIR = libft
HEAD = filler.h
INC = $(addsuffix /includes, $(CURDIR))
INC_MLX = /usr/local/include

all: $(NAME)
	$(MAKE) -C $(CURDIR)

$(NAME): $(OBJ)
	$(MAKE) -C $(CURDIR)
	$(CC) $(CFLAGS) -I $(INC) -o $@ $(CURDIR)/$(addsuffix .a, $(CURDIR)) $^

libft.a: ololo
	$(MAKE) -C $(CURDIR)/

ololo:
	true

visual: $(OBJ_V)
	$(MAKE) -C $(CURDIR)
	$(CC) $(CFLAGS) -I $(INC) -L /usr/local/lib -lmlx -framework OpenGL -framework AppKit $(OBJ_S) -o $@ $(CURDIR)/$(addsuffix .a, $(CURDIR)) $^


clean:
	rm -f $(OBJ)
	rm -f $(OBJ_V)
	$(MAKE) -C $(CURDIR) clean

fclean: clean
	rm -f $(NAME)
	rm -f visual
	$(MAKE) -C $(CURDIR) fclean

re: fclean all

$(OBJ) : %.o: %.c
	$(CC) -I $(INC) -c $(CFLAGS) $< -o $@

$(OBJ_V) : %.o: %.c
	$(CC) $(CFLAGS) -I $(INC_MLX) -c $< -o $@

$(OBJ): $(HEAD)

$(OBJ_V): $(HEAD) $(OBJ_S)
