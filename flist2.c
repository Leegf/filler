/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flist2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/17 13:13:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/22 16:19:14 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

#define VAL len

static int g_flag;

void	merge_part_2(t_flist **l, t_flist **r, t_flist **head)
{
	while (*l && *r)
	{
		if ((g_flag = ((*l)->VAL < (*r)->VAL)))
			(*head)->next = *l;
		else
			(*head)->next = *r;
		if (g_flag)
			(*l) = (*l)->next;
		else
			(*r) = (*r)->next;
		(*head) = (*head)->next;
	}
	while (*l)
	{
		(*head)->next = *l;
		(*head) = (*head)->next;
		*l = (*l)->next;
	}
	while (*r)
	{
		(*head)->next = *r;
		(*head) = (*head)->next;
		*r = (*r)->next;
	}
}

void	merge_flist(t_flist *l, t_flist *r, t_flist **head)
{
	t_flist tmp;

	*head = NULL;
	if (!l || !r)
	{
		*head = l == NULL ? r : l;
		return ;
	}
	if (l->VAL < r->VAL)
	{
		*head = l;
		l = l->next;
	}
	else
	{
		*head = r;
		r = r->next;
	}
	tmp.next = *head;
	merge_part_2(&l, &r, head);
	*head = tmp.next;
}

void	flist_clear(t_flist **alst)
{
	t_flist *tmp;

	if (!alst || !(*alst))
		return ;
	while (*alst)
	{
		tmp = *alst;
		(*alst) = (*alst)->next;
		free(tmp);
		tmp = NULL;
	}
	*(alst) = NULL;
}

void	fill_coords(t_options *ord, t_flist *lst, int i, int j)
{
	(*ord).cor.x = lst->cor_x;
	(*ord).cor.y = lst->cor_y;
	(*ord).cor_t.x = j;
	(*ord).cor_t.y = i;
}

void	cpy_struct(t_options *opt, t_options ord)
{
	(*opt).cor.x = ord.cor.x;
	(*opt).cor.y = ord.cor.y;
	(*opt).cor_t.x = ord.cor_t.x;
	(*opt).cor_t.y = ord.cor_t.y;
	give_the_coords(opt->cor, opt->cor_t);
}
