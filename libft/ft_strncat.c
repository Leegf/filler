/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 17:28:49 by lburlach          #+#    #+#             */
/*   Updated: 2017/11/10 16:24:35 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** strncat() function appends not more than n characters from s2, and
** then adds a terminating `\0'.
*/

char	*ft_strncat(char *s1, const char *s2, size_t n)
{
	size_t len;
	size_t i;

	i = 0;
	len = ft_strlen(s1);
	while (s2[i] && i < n)
		s1[len++] = s2[i++];
	s1[len] = '\0';
	return (s1);
}
