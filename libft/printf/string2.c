/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/20 15:33:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/02/04 17:25:33 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** Okay, I know, it's awfully done. Corrects the number of spaces for width
** if both precision and width are given.
*/

void	correct_width_2(t_flags *flags, wchar_t *st, int s)
{
	unsigned int	size;
	int				i;
	int				prec;
	int				num;

	i = 0;
	prec = (*flags).prec;
	while (42)
	{
		size = count_those_great_bits(st[i]);
		num = num_of_bytes(size);
		if ((num == 1 && prec >= 1) || (num == 2 && prec >= 2) ||
				(num == 3 && prec >= 3) || (num == 4 && prec >= 4))
			size = 1;
		else
			break ;
		prec -= num;
		(*flags).width -= num;
		i++;
		if (!(st[i]) || prec < 1)
			break ;
	}
	(*flags).width = (*flags).width + s;
}

/*
** It sends every character of w_char to char_unicode function which makes
** all dirty work done. It considers precision.
*/

void	stuff_uc_prec(t_list **head, wchar_t *st, int prec)
{
	unsigned int	size;
	int				rub;

	while (42)
	{
		size = count_those_great_bits(*st);
		if (size <= 7)
			rub = 1;
		else if (size <= 11)
			rub = 2;
		else if (size <= 16)
			rub = 3;
		else
			rub = 4;
		if (prec / rub < 1 || !(*st))
			break ;
		parse_uc(size, (unsigned int)*st, head);
		prec -= rub;
		st++;
	}
}

/*
** Forming an unicode_string. If precision is not noted, it simply sends
** character to char_unicode function.
*/

void	form_us(t_flags flags, wchar_t *st, t_list **head)
{
	unsigned int size;

	if (flags.prec >= 0)
		stuff_uc_prec(head, st, flags.prec);
	else
	{
		while (*st)
		{
			size = count_those_great_bits(*st);
			parse_uc(size, (unsigned int)*st, head);
			st++;
		}
	}
}

/*
** Parsing of unicode string begins here. Very much hope it won't be very much
** painful.
*/

void	write_l_string(t_flags *flags, wchar_t *st, t_list **head, int s)
{
	if ((*flags).prec < 0 || (int)count_all_bytes(st) < (*flags).prec)
		correct_width(flags, st);
	else
		correct_width_2(flags, st, s);
	if ((*flags).minus)
	{
		form_us((*flags), st, head);
		form_width_s((*flags), head, s);
	}
	else
	{
		form_width_s((*flags), head, s);
		form_us((*flags), st, head);
	}
}
