/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/28 19:33:25 by lburlach          #+#    #+#             */
/*   Updated: 2018/01/28 19:33:28 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <stdio.h>

/*
** It transforms our t_list buff into a string which will go to the standart out
** put. TODO: Don't forget to free the buff once it's gone to the output.
*/

ssize_t	allocate(t_list *tmp, char **line)
{
	t_list			*beg;
	size_t			i;
	size_t			c;

	c = 0;
	beg = tmp;
	while (beg)
	{
		c += beg->content_size;
		beg = beg->next;
	}
	(*line) = ft_memalloc(c + 1);
	if (*line == NULL)
		return (-1);
	i = 0;
	while (tmp)
	{
		ft_memcpy(&(*line)[i], tmp->content, tmp->content_size);
		i += (tmp->content_size);
		tmp = tmp->next;
	}
	return (c);
}

/*
** Here, magic happens. My attempt to use array of pointers, first.
** Plus, my endeavour to implement all args' parsing.
*/

void	writing_variadic_args(va_list ax, t_flags flags, t_list **head)
{
	const static t_wrti	arr[] = {
			{"cC", &write_char, NULL},
			{"sS", NULL, &write_string},
			{"dDi", &write_int_i, &write_int_l}
	};
	const static t_wrtl	arr2[] = {
			{"uUb", &write_u_i, &write_u_l},
			{"oO", &write_o_i, &write_o_l},
			{"xXp", &write_x_i, &write_x_l}
	};

	if (!flags.perc || !flags.conv)
		return ;
	else if (flags.conv == '%')
		write_char(&flags, '%', head);
	else if (!check_conv(flags.conv))
		write_char(&flags, flags.conv, head);
	else
	{
		f_p_parsing(ax, &flags, head, arr);
		s_p_parsing(ax, &flags, head, arr2);
	}
}

/*
** The name of the function is rather self-explanatory, isn't it?
*/

int		parsing(va_list ax, char *fmt, t_list **head)
{
	char	*buff_str;
	t_flags	flags;
	ssize_t	len;

	buff_str = NULL;
	while (*fmt)
	{
		initialise_flags(&flags);
		parse_simple_str(&fmt, head, &flags);
		parse_flags(&flags, &fmt);
		parse_width_n_prec(&flags, &fmt, ax);
		parse_length_n_conversion(&flags, &fmt);
		writing_variadic_args(ax, flags, head);
	}
	len = allocate(*head, &buff_str);
	if (len == -1)
		return (-1);
	ft_lst_clear(head);
	write(1, buff_str, len);
	ft_strdel(&buff_str);
	return (len);
}

/*
** Pls, work.
*/

int		ft_printf(const char *format, ...)
{
	va_list	ax;
	t_list	*buff;
	int		res;

	buff = NULL;
	va_start(ax, format);
	res = parsing(ax, (char *)format, &buff);
	va_end(ax);
	return (res);
}
