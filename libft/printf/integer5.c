/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   integer5.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/04 16:30:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/02/04 16:30:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		check_flags(t_flags flags)
{
	if (flags.conv == 'o' || flags.conv == 'O' || flags.conv == 'x'
		|| flags.conv == 'X' || flags.conv == 'p')
		return (1);
	return (0);
}
