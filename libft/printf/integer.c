/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   integer.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 20:11:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/01/23 20:11:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** Stuffs a string with spaces or 0. For width and prec.
*/

void	stuff_it(int s, t_list **head, t_flags flags, int ultra_f)
{
	char	*str;
	int		i;
	char	chr;

	if (s < 1)
		return ;
	i = 0;
	if (ultra_f)
		chr = flags.zero == 1 && flags.prec == -1 && !flags.minus ? '0' : ' ';
	else
		chr = '0';
	str = ft_memalloc(s);
	while (i < s)
		str[i++] = chr;
	ft_lst_push_back(head, str, s);
	ft_strdel(&str);
}

/*
** Counts and checks whether it'd be needed to make
** smth with width.
*/

int		count_that_shit2(t_flags flags, char sign, int s, char *n)
{
	int num;

	num = s;
	sign = sign + 1;
	if (flags.space && flags.width > s && !flags.minus)
		num += 1;
	if (flags.prec > s)
	{
		num += flags.prec - s;
		if (flags.hash && ((flags.conv == 'o') || flags.conv == 'O'))
			num -= 1;
	}
	if (n[0] != '0' && flags.hash && (flags.conv == 'o' || flags.conv == 'O'))
		num += 1;
	if (flags.hash && flags.conv == 'p')
		num += 2;
	if (n[0] != '0' && flags.hash && (flags.conv == 'x' || flags.conv == 'X'))
		num += 2;
	if (flags.hash && !flags.point && n[0] == '0'
		&& (flags.conv == 'o'
			|| flags.conv == 'O') && flags.length < 3)
		num += 1;
	return (num);
}

int		count_that_shit(t_flags flags, char sign, int s, char *n)
{
	int num;

	if (check_flags(flags))
		return (count_that_shit2(flags, sign, s, n));
	num = s;
	if ((flags.space && sign != '-' && !flags.plus)
		|| (flags.plus && sign != '-') || (sign == '-'))
		num += 1;
	if (!flags.space && flags.plus && sign != '-' && (flags.conv == 'u'
													|| flags.conv == 'U'))
		num -= 1;
	if (flags.minus && flags.space && (flags.conv == 'u' || flags.conv == 'U'))
		num -= 1;
	if (flags.prec > s)
		num += flags.prec - s;
	return (num);
}

/*
** Forming width, yup...
*/

void	form_width_c2(t_flags *flags, t_list **head, int s, char *n)
{
	char	sign;
	int		num;

	sign = n[0] == '-' ? '-' : '+';
	if (check_flags((*flags)))
		num = count_that_shit2((*flags), sign, s, n);
	else
		num = count_that_shit((*flags), sign, s, n);
	if ((*flags).zero && (*flags).width > num && !(*flags).minus
		&& (*flags).prec == -1)
		make_zero_work(flags, head, sign, n);
	if ((*flags).zero && (*flags).width - num > 0 && (*flags).prec == -1
		&& !(*flags).minus && !fk_p((*flags), sign))
		hex_fix(flags, head, n);
	if ((*flags).zero && (*flags).hash && (*flags).space && !(*flags).minus
		&& ((*flags).conv == 'x' || (*flags).conv == 'X'))
		hex_fix(flags, head, n);
	if (n[0] == '0' && (*flags).conv == 'p'
		&& ((*flags).prec == 0 || ((*flags).point && (*flags).prec == -1)))
		num -= 1;
	if (n[0] == '0' && (*flags).width > (*flags).prec && ((*flags).conv == 'o'
						|| (*flags).conv == 'O') && (*flags).length == 42)
		num += 1;
	stuff_it((*flags).width - num, head, (*flags), 1);
}

/*
** I'm not god at naming functions, I think... Well, it's
** the ending func for forming a string made of int.
*/

void	to_the_exit(t_flags flags, t_list **head, char *n, int s)
{
	char sign;

	sign = n[0] == '-' ? '-' : '+';
	if ((flags.conv == 'o' || flags.conv == 'O') && n[0] == '0' &&
			!flags.width && (flags.prec != 0 || (!flags.point
												&& flags.prec != -1)))
		ft_lst_push_back(head, "0", 1);
	else if (n[0] == '0' && (flags.conv == 'o' || flags.conv == 'O')
			&& flags.hash
		&& ((flags.prec < 2) || (flags.point && flags.prec == -1)))
		ft_lst_push_back(head, "0", 1);
	else if (flags.conv != 'p' && n[0] == '0' && !flags.width
			&& ((flags.prec == 0) || (flags.point && flags.prec == -1)))
		ft_lst_push_back(head, "", 0);
	else if (flags.conv != 'p' && n[0] == '0' && flags.width
			&& ((flags.prec == 0)))
		ft_lst_push_back(head, " ", 1);
	else if (sign == '-')
		ft_lst_push_back(head, &(n[1]), s);
	else
		to_exit_2(flags, head, n, s);
}
