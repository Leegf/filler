/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flist.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/17 13:11:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/22 15:49:25 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

t_flist		*ft_flstnew(int x, int y, int l)
{
	t_flist *tmp;

	tmp = (t_flist*)malloc(sizeof(t_flist));
	if (tmp == NULL)
		return (NULL);
	tmp->cor_x = x;
	tmp->cor_y = y;
	tmp->len = l;
	tmp->next = NULL;
	return (tmp);
}

void		ft_push_flist(t_flist **tmp, int x, int y, int l)
{
	t_flist *hm;
	t_flist *kek;

	if (!tmp)
		return ;
	kek = *tmp;
	hm = ft_flstnew(x, y, l);
	if (hm == NULL)
		return ;
	if (!(*tmp))
	{
		*tmp = hm;
		return ;
	}
	while (kek->next)
		kek = kek->next;
	kek->next = hm;
}

void		split_flist(t_flist *head, t_flist **l, t_flist **r)
{
	t_flist *hm1;
	t_flist *hm2;

	if (head == NULL || head->next == NULL)
	{
		*l = head;
		*r = NULL;
		return ;
	}
	hm1 = head;
	hm2 = head->next;
	while (hm2)
	{
		hm2 = hm2->next;
		if (hm2)
		{
			hm2 = hm2->next;
			hm1 = hm1->next;
		}
	}
	*l = head;
	*r = hm1->next;
	hm1->next = NULL;
}

void		sort_flist(t_flist **head)
{
	t_flist *l;
	t_flist *r;

	l = NULL;
	r = NULL;
	if ((*head == NULL) || ((*head)->next == NULL))
		return ;
	split_flist(*head, &l, &r);
	sort_flist(&l);
	sort_flist(&r);
	merge_flist(l, r, head);
}
